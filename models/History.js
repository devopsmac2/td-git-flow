const mongoose = require('mongoose');

const historySchema = new mongoose.Schema({

  operation: String,
  createdAt: Date,
});


const History = mongoose.model('History', historySchema);

module.exports = History;
