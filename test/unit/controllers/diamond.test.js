describe("Module Diamond", () => {
  describe("GET /diamond", function() {
    it('should redirect to /diamond/new', function() {
      return server.run('/diamond/new').then((res) => {
        res.statusCode.should.equal(200);
      });
    })
  })

  describe("GET /diamond/new", function() {
    it('should show a form with an input and a submit', function() {

      return server.run('/diamond').then((res) => {

        //res.request.body.should.contains('<input>')
        //res.request.body.should.contains('<submit>')
      })
    })
  })

  describe("POST /diamond", function() {

    it('should reject when `width` parameter is not a number', function() {

      return server.run({
        method: 'post',
        url: '/diamond',
        payload: { width: 'a' }
      }).then(function(res) {
        res.statusCode.should.equal(400)
      })
    });

    // odd = impair
    it('should reject when `width` parameter is not odd', function() {

      return server.run({
        method: 'post',
        url: '/diamond',
        payload: { width: 2 }
      }).then(function(res) {
        res.statusCode.should.equal(400)
      })
    });

    testDiamonds = [{
      width: 1,
      result: '+'
    }, {
      width: 3,
      result: [
        '.+.',
        '+++',
        '.+.'
      ].join("\n")
    }, {
      width: 5,
      result: [
        '..+..',
        '.+++.',
        '+++++',
        '.+++.',
        '..+..'
      ].join("\n")
    }]

    // C'est possible de créér des tests unitaires de façon dynamique
    testDiamonds.forEach(function(diamond){
      it(`should show correct result on width = ${diamond.width}`, function() {
        return server.run({
          method: 'post',
          url: '/diamond',
          payload: { width: diamond.width }
        }).then(function(res) {
          res.statusCode.should.equal(200)
          res.result.should.equal(diamond.result)
        })
      })
    })

    it('should invert + and . when given `width` is negative', function() {

    })
  })
})
