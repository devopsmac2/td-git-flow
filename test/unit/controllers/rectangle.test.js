describe("Module Rectangle", () => {
  describe("GET /rectangle", function() {
    it('should create a diagram of 4 columns with 3,2,4,1 params', function() {
        return server.run('/rectangle/3,2,4,1').then((res)=>{
          res.result.should.equal(
            [
              '4     $   ',
              '3 $   $   ',
              '2 $ $ $   ',
              '1 $ $ $ $ ',
              '+ 1 2 3 4 ',
            ].join('\n')
          )
        })
    })
  })

  describe("GET /rectangle/hauteur/largeur", function(){
    it('should get the page', function(){
      return server.run('/rectangle/4/5').then((res)=>{
        res.statusCode.should.equal(200);
      })
    })
  })
})
