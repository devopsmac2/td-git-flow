# README #

### Présentation de l'équipe ###
* Maxime Boisse
* Florent Pailhes
* Maxime Zed
* Baptiste Yung
* Louis Rejou

### Ce qui c'est mal passé ###

* Quick summary
* Version
* Hapi et la création de vues
### Ce qui c'est bien passé ###
* Ticket DO-27 [v0.2.0]
* Ticket DO-51 [v0.3.0]
* Ticket DO-52 [v0.3.0]
* Ticket DO-155 [v1.1.0]
### Ce qui c'est aurait fallu faire ###
* Ticket DO-60 [v1.0.0]
### Tableau Kaizen ###

# Etat du projet : #

### Ce qui est fait ###
      * Ticket DO-27 [v0.2.0]
      * Ticket DO-51 [v0.3.0]
      * Ticket DO-52 [v0.3.0]
      * Ticket DO-53 [v0.4.0]
      * Ticket DO-60 [v1.0.0]
      * Ticket DO-79 [v.2.1]
      * Ticket DO-150 [v1.1.0]
      * Ticket DO-155 [v1.1.0]

      * Prise en charge de Grafana : http://ec2-52-33-201-84.us-west-2.compute.amazonaws.com:3000
            login : admin  
            mdp : admin 
### Ce qui reste à faire ###

      * Ticket DO-61 [v1.0.0]
      * Ticket DO-161 [v1.2.0]
      * Ticket DO-172 [v1.1.1]
      * Ticket DO-28 [v0.2.0]
      * Ticket DO-29 [v0.2.0]
      * Ticket DO-30 [v0.3.0]
      * Ticket DO-35 [v1.0.0] [ops only]

### Ce qui ne fonctionne pas ###
