// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom');

module.exports.index = function(request, reply){
  reply.redirect('/diamond/new');
}

module.exports.showForm = function(request, reply){

  reply('<html><head><title>Diamond</title></head><body>' +
        '<form action="/diamond" method="post">'+
        '<input type="text" name="width"  /><input type="submit" value="Submit" /></form>' +
        '</body></html>');
}


module.exports.generate = function(request, reply){
  console.log(request.payload.width) // Show POST parameters

  width = Math.round(parseFloat(request.payload.width));

  if(isNaN(width)) {
    return reply(Boom.badRequest('Width is not a number.'));
  }else if (Math.abs(width % 2) != 1) {
    return reply(Boom.badRequest('Width is not a odd.'));
  }

  var diamond = {
      width: width,
      result: []
  };

  var str = "";

  if(width>0){

    const createLine = (i, width) => {
        var diams = "";
        var space = "";
        var result = [];
        for (let x = 0; x < i; x++) {
            diams += "+";
        }
        for (let z = 0; z < (width - i) / 2; z++) {
            space += ".";
        }
        str = space + diams+ space;

        diamond.result.push(str);

    };

    for (let i = 1; i < width; i += 2) {

      createLine(i, width);
    }

    for (let i = width; i > 0; i -= 2) {
      createLine(i, width);
    }
    diamond.result.join('\n')
  }
  else
  {
    var width = width * -1;
    const createLine = (i, width) => {
        var diams = "";
        var space = "";
        var result = [];
        for (let x = 0; x < i; x++) {
            diams += ".";
        }
        for (let z = 0; z < (width - i) / 2; z++) {
            space += "+";
        }
        str = space + diams+ space;

        diamond.result.push(str);

    };

    for (let i = 1; i < width; i += 2) {

      createLine(i, width);
    }

    for (let i = width; i > 0; i -= 2) {
      createLine(i, width);
    }
    diamond.result.join('\n')
  }

  reply(diamond.result.join('\n'));
}
