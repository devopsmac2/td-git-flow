// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom');

module.exports.generate = function(request, reply){
  tab = request.params.tab.split(',');
  base = '+ '
  result = []

  for (var i = 0; i < Math.max.apply(null, tab); i++) {
    result[i] = Math.max.apply(null, tab)-i +' '
  }

  tab.forEach(function(value, index){
    base += (parseInt(index)+1)+' '
    result.forEach(function(valueR, indexR){
      realIndex = result.length - indexR -1
      if (indexR < value) {
        result[realIndex] += '$ '
      }else{
        result[realIndex] += '  '
      }
    })
  })

  result.push(base);


  // result.push("");
  // result.push("Aire max : "+Math.max.apply(null, tab));
  reply(result.join('\n'))
}

module.exports.randomTab = function(request, reply){
  height = request.params.height;
  width = request.params.width;
  tab = []

  min = Math.ceil(0);
  max = Math.floor(height);
  for (var i = 0; i < width; i++) {
    random = Math.floor(Math.random() * (max - min +1)) + min;

    tab.push(random)
  }

  base = '+ '
  result = []

  for (var i = 0; i < Math.max.apply(null, tab); i++) {
    result[i] = Math.max.apply(null, tab)-i +' '
  }

  tab.forEach(function(value, index){
    base += (parseInt(index)+1)+' '
    result.forEach(function(valueR, indexR){
      realIndex = result.length - indexR -1
      if (indexR < value) {
        result[realIndex] += '$ '
      }else{
        result[realIndex] += '  '
      }
    })
  })

  result.push(base);
  result.push("");
  result.push("Aire max : "+Math.max.apply(null, tab));
  reply(result.join('\n'))
}
