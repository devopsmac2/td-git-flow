const
  path = require('path')
  mongoConfig = require(path.join(__dirname, '..', 'config')).mongo,
  dns = require('dns'),
  os = require('os'),
  MongoClient = require('mongodb').MongoClient,
  mongoose = require('mongoose');

let
  ipAdress = null,
  seen = 0,
  mongoStatus = 'not working';

mongoose.Promise = global.Promise;

MongoClient.connect(
  `mongodb://${mongoConfig.host}:${mongoConfig.port}/${mongoConfig.database}`,
  function(err, db) {
    if(err) {
      console.log('Mongo ERR: ', err)
      return;
    }
    mongoStatus = 'working';
    db.close();
  }
);


dns.lookup(os.hostname(), function(err, ip) {
  ipAdress = ip;
})

module.exports = function(request, reply){
  seen++;
  reply('Hello world!');
}
