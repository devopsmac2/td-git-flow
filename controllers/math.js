// HAPI utilise Boom pour la gestion d'erreurs
const Boom = require('boom');
const History = require('../models/History');



module.exports.getAdd = function(request, reply){
  // parseFloat convert anything to either a float number or NaN
  term1 = Math.round(parseFloat(request.params.term1)*100)
  term2 = Math.round(parseFloat(request.params.term2)*100)

  if(isNaN(term1) || isNaN(term2)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'));
  }

  var result = (term1 + term2)/100;

  var history = new History({
    operation: term1+' + '+term2+' = '+result,
    createdAt: new Date(),
  });

  history.save((err) => {
    if (err) { return next(err); }
  });
  reply(result);
}

module.exports.getSubstraction = function(request, reply){
  // parseFloat convert anything to either a float number or NaN
  term1 = parseFloat(request.params.term1);
  term2 = parseFloat(request.params.term2);

  if(isNaN(term1) || isNaN(term2)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'));
  }

  var result = term1 - term2;

  var history = new History({
    operation: term1+' - '+term2+' = '+result,
    createdAt: new Date(),
  });

  history.save((err) => {
    if (err) { return next(err); }
  });
  reply(result);
}

module.exports.getDivide = function(request, reply){
  dividend = parseFloat(request.params.dividend);
  divisor = parseFloat(request.params.divisor);

  if(isNaN(dividend) || isNaN(divisor)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'));
  }

  if(divisor === 0) {
    return reply(Boom.badRequest('Divisor can not be 0.'))
  }

  result = (dividend / divisor)

  var history = new History({
    operation: dividend +' / '+divisor+' = '+result,
    createdAt: new Date(),
  });

  history.save((err) => {
    if (err) { return next(err); }
  });
  reply(result);

}

module.exports.getMultiplication = function(request, reply){
  // parseFloat convert anything to either a float number or NaN
  term1 = parseFloat(request.params.term1);
  term2 = parseFloat(request.params.term2);

  if(isNaN(term1) || isNaN(term2)) {
    return reply(Boom.badRequest('At least one of the term is not a number.'));
  }

  var result = term1 * term2;

  var history = new History({
    operation: term1+' * '+term2+' = '+result,
    createdAt: new Date(),
  });

  history.save((err) => {
    if (err) { return next(err); }
  });
  reply(result);
}

module.exports.getHistory = function(request, reply){

  History.find({}, (err, data) => {
    reply(data);
  });
}
